provider "kubernetes" {

    //host = data.aws_eks_cluster.myapp-cluster.endpoint
    //token = data.aws_eks_cluster_auth.myapp-cluster.token
    //cluster_ca_certificate = base64decode(data.aws_eks_cluster.myapp-cluster.certificate_authority.0.data)

    host = module.myapp-eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.myapp-eks.cluster_certificate_authority_data)

}

/*

    data "aws_eks_cluster" "myapp-cluster" {
        name = module.myapp-eks.cluster_id
    }

    data "aws_eks_cluster_auth" "myapp-cluster" {
        name = module.myapp-eks.cluster_id
    }

*/


module "myapp-eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.1.1"

  cluster_name = "myapp-eks"
  cluster_version = "1.23"

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access = true
  enable_irsa = true


  subnet_ids = module.myapp-vpc.private_subnets
  vpc_id = module.myapp-vpc.vpc_id



  tags = {
    environment = "development"
    application = "myapp"
  }

  eks_managed_node_groups = {

    myapp-node = {
        desired_size = 2
        min_size = 1
        max_size = 3

        instance_types = ["t2.micro"]
        capacity_type = "ON_DEMAND"
    }

  }

}