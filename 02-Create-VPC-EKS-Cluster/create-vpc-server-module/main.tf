provider "aws" {
    region = "ap-southeast-2"
}


resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

module "myapp-subnet" {
    source = "./modules/subnet"
    vpc_id = aws_vpc.myapp-vpc.id
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

}

module "myapp-server" {
    source = "./modules/server"
    image_name = var.image_name
    vpc_id = aws_vpc.myapp-vpc.id
    my_ip = var.my_ip
    env_prefix = var.env_prefix
    public_key = var.public_key
    instance_type = var.instance_type
    avail_zone = var.avail_zone
    subnet_id = module.myapp-subnet.subnet.id
}
