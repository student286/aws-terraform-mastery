# VPC Variables
vpc_name = "myvpc"
vpc_cidr_block = "10.0.0.0/16"
vpc_availability_zones = ["ap-southeast-1a", "ap-southeast-1b"]
vpc_public_subnets = ["10.0.1.0/24", "10.0.10.0/24"]
vpc_private_subnets = ["10.0.20.0/24", "10.0.30.0/24"]
vpc_database_subnets= ["10.0.40.0/24", "10.0.50.0/24"]
vpc_create_database_subnet_group = true 
vpc_create_database_subnet_route_table = true   
vpc_enable_nat_gateway = true  
vpc_single_nat_gateway = true